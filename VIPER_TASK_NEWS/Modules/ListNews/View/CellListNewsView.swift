//
//  CellListNewsView.swift
//  VIPER_TASK_NEWS
//
//  Created by Nguyen Minh Tri on 12/12/17.
//  Copyright © 2017 Nguyen Minh Tri. All rights reserved.
//

import UIKit

class CellListNewsView: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var titleView: UILabel!
    
    @IBOutlet weak var timeView: UILabel!
    
    

}
